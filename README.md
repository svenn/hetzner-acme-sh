# hetzner-acme.sh
An [acme.sh](https://github.com/acmesh-official/acme.sh)-extension that makes a DNS-01 challenge verification possible for zones that are hosted at dns.hetzner.com
