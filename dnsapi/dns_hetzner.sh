#!/usr/bin/env sh

# Author: Sven Masuhr <mail at svenn dot me>
# Repository: https://gitlab.com/svenn/hetzner-acme-sh

#HETZNERDNS_API_TOKEN=XXXXX
HETZNERDNS_API="https://dns.hetzner.com/api/v1"

########## public ##########

#Usage: dns_hetzner_add _acme-challenge.www.domain.tld "VALUE"
dns_hetzner_add() {
  _info "Using Hetzner DNS."

  if ! _dns_hetzner_firststart; then
    return 1
  fi

  dns_zone="$(_dns_hetzner_get_zone "$1")"
  if [ -z "$dns_zone" ]; then
    _err "Zone for (Sub-)Domain '${1}' does not exist!'"
    return 1
  fi

  dns_txt_host=$(echo "$1" | sed "s/.$dns_zone//g")
  dns_txt_value=$2
  
  _debug dns_zone "$dns_zone"
  _debug dns_txt_host "$dns_txt_host"
  _debug dns_txt_value "$dns_txt_value"
  
  _info "Creating TXT record for $1" 

  create_response=$(_dns_hetzner_create_txt_record "$dns_zone" "$dns_txt_host" "$dns_txt_value")
  
  if [ "$create_response" != "record created" ];
  then
    _err "TXT-Record could not be created!"
    return 1
  fi
  
  _info "TXT-Record created!"
  return 0

}

#Usage: dns_hetzner_rm _acme-challenge.www.domain.tld "VALUE"
dns_hetzner_rm() {
 _info "Using Hetzner DNS."

  if ! _dns_hetzner_firststart; then
    return 1
  fi

  dns_zone="$(_dns_hetzner_get_zone "$1")"
  if [ -z "$dns_zone" ]; then
    _err "Zone for (Sub-)Domain '${1}' does not exist!'"
    return 1
  fi

  dns_txt_host=$(echo "$1" | sed "s/.$dns_zone//g")
  dns_txt_value=$2

  _debug dns_zone "$dns_zone"
  _debug dns_txt_host "$dns_txt_host"
  _debug dns_txt_value "$dns_txt_value"

  dns_txt_record_id=$(_dns_hetzner_get_txt_record_id "$dns_zone" "$dns_txt_host" "$dns_txt_value")
  
  if [ -z "$dns_txt_record_id" ]; then
    _err "TXT-Record could not be found!"
    return 1
  fi
  
  _debug dns_txt_record_id "$dns_txt_record_id"
  
  dns_txt_record_removal=$(_dns_hetzner_remove_txt_record_by_id "$dns_txt_record_id")
  
  if [ "$dns_txt_record_removal" != "record removed" ]; then
    _err "TXT-Record could not be removed!"
    return 1
  fi  
  
  _info "Record successfully removed."
  return 0
}

########## private ##########

_dns_hetzner_firststart() {

  if [ -n "$HETZNERDNS_INIT_COMPLETED" ]; then
    return 0
  fi

  HETZNERDNS_API_TOKEN="${HETZNERDNS_API_TOKEN:-$(_readaccountconf_mutable HETZNERDNS_API_TOKEN)}"
  
  if [ -z "$HETZNERDNS_API_TOKEN" ]; then
    _err "API token for Hetzner-DNS has not been specified!"
    _err "Please specify a API-Token to be used and try again."
    return 1
  fi

  _debug init "true"

  _dns_hetzner_api_request "GET" "/zones"

  if _contains "$response" "\"message\":\"Invalid authentication credentials\""; then
    _err "Invalid API-Token!"
    return 1
  fi

  _saveaccountconf_mutable HETZNERDNS_API_TOKEN "$HETZNERDNS_API_TOKEN"

  HETZNERDNS_INIT_COMPLETED=1
  return 0
}


_dns_hetzner_create_txt_record() {

  dns_zone=$1
  dns_txt_host=$2
  dns_txt_value=$3
 
  _dns_hetzner_api_request "POST" "/records" "{\"zone_id\": \"$dns_zone\",\"type\": \"TXT\",\"name\": \"$dns_txt_host\",\"value\": \"$dns_txt_value\",\"ttl\": 0}"
  
  if _contains "$response" "\"message\":\"invalid argument\""; then
    _err "Invalid Record Data for creation!"
    return 1
  fi
  echo "record created"
  return 0
}

_dns_hetzner_remove_txt_record_by_id() {
  record_id=$1
  
  _dns_hetzner_api_request "DELETE" "/records/$record_id"
  
  if _contains "$response" "\"message\":\"record not found\""; then
    _err "Invalid Record ID!"
    return 1
  fi
  echo "record removed"
  return 0
}


_dns_hetzner_get_txt_record_id() {
  dns_zone=$1
  dns_txt_host=$2
  dns_txt_value=$3
  
  _dns_hetzner_api_request "GET" "/records" "zone_id=$dns_zone"
  _debug response "$response"
  
  matches=$(echo "$response" | sed 's/},{/\n/g' | sed "s/\"/'/g" | grep -Po "'id':'.*','type':'TXT','name':'$dns_txt_host','value':'$dns_txt_value'" | grep -Po "'id': *\K'[^']*'" | sed "s/'//g")
  
  for id in $matches
  do
    echo "$id"
    return 0
  done
}

_dns_hetzner_get_zone() {
  fqdn=$1

  _dns_hetzner_api_request "GET" "/zones"
  zones=$(echo "$response" | grep -Po '"id":.*?[^\\]"name":.*?[^\\]"ttl"' | grep -Po '"name": *\K"[^"]*"' | sed 's/"//g' | sed 's/},{/}/g')
  
  set -a array "$zones"

  for zone in $zones
  do
    if _endswith "$fqdn" "$zone";
    then
      _debug zone "$zone"
      echo "$zone"
      return 0
    fi
  done
  _debug zonenotfound "true"
  return 1
}

_dns_hetzner_api_request() {
  method=$1
  api_call=$2
  data=$3 
  
  hetznerdns_api_token=$(_readaccountconf_mutable HETZNERDNS_API_TOKEN)
  
  _debug hetznerdns_api_token "$hetznerdns_api_token"

  export _H1="Auth-API-Token: $hetznerdns_api_token"
  export _H2="Content-Type: application/json; charset=utf-8"

  _debug HEADER1 "$_H1"
  _debug HEADER2 "$_H2"
  
  case $method in
    GET)
      if [ -z "$data" ]; then
        response=$(_get "${HETZNERDNS_API}${api_call}")
      else
        response=$(_get "${HETZNERDNS_API}${api_call}?${data}")
      fi
      _debug response "$response"
      return 0
      ;;
    POST)
      response=$(_post "$data" "${HETZNERDNS_API}${api_call}")
      _debug response "$response"
      return 0
      ;;    
    DELETE)
      response=$(_post "$data" "${HETZNERDNS_API}${api_call}" "" "DELETE")
      _debug response "$response"
      return 0
      ;;
  esac

  return 1
}
